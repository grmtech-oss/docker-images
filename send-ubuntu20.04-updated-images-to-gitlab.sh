#!/bin/bash
cd /gt/gt-prog-repos/docker-images/ubuntu20.04/
docker build --no-cache -t registry.gitlab.com/grmtech-oss/docker-images/ubuntu20.04 .
docker push registry.gitlab.com/grmtech-oss/docker-images/ubuntu20.04-nginx

cd /gt/gt-prog-repos/docker-images/ubuntu20.04-nginx/
docker build --no-cache -t registry.gitlab.com/grmtech-oss/docker-images/ubuntu20.04-nginx .
docker push registry.gitlab.com/grmtech-oss/docker-images/ubuntu20.04-nginx

cd /gt/gt-prog-repos/docker-images/ubuntu20.04-nginx-gulp/
docker build --no-cache -t registry.gitlab.com/grmtech-oss/docker-images/ubuntu20.04-nginx-gulp .
docker push registry.gitlab.com/grmtech-oss/docker-images/ubuntu20.04-nginx-gulp

cd /gt/gt-prog-repos/docker-images/ubuntu20.04-nginx-gulp-php/
docker build --no-cache -t registry.gitlab.com/grmtech-oss/docker-images/ubuntu20.04-nginx-gulp-php .
docker push registry.gitlab.com/grmtech-oss/docker-images/ubuntu20.04-nginx-gulp-php

cd /gt/gt-prog-repos/docker-images/ubuntu20.04-php/
docker build --no-cache -t registry.gitlab.com/grmtech-oss/docker-images/ubuntu20.04-php .
docker push registry.gitlab.com/grmtech-oss/docker-images/ubuntu20.04-php
