#!/bin/bash
docker pull registry.gitlab.com/grmtech-oss/docker-images/ubuntu20.04:latest
docker pull registry.gitlab.com/grmtech-oss/docker-images/ubuntu20.04-nginx:latest
docker pull registry.gitlab.com/grmtech-oss/docker-images/ubuntu20.04-nginx-gulp:latest
docker pull registry.gitlab.com/grmtech-oss/docker-images/ubuntu20.04-nginx-gulp-php:latest
docker pull registry.gitlab.com/grmtech-oss/docker-images/ubuntu20.04-php:latest
