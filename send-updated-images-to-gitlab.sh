#!/bin/bash
cd /gt/gt-prog-repos/docker-images/ubuntu/
docker build --no-cache -t registry.gitlab.com/grmtech-oss/docker-images/ubuntu .
docker push registry.gitlab.com/grmtech-oss/docker-images/ubuntu

cd /gt/gt-prog-repos/docker-images/ubuntu-nginx/
docker build --no-cache -t registry.gitlab.com/grmtech-oss/docker-images/ubuntu-nginx .
docker push registry.gitlab.com/grmtech-oss/docker-images/ubuntu-nginx

cd /gt/gt-prog-repos/docker-images/ubuntu-nginx-gulp/
docker build --no-cache -t registry.gitlab.com/grmtech-oss/docker-images/ubuntu-nginx-gulp .
docker push registry.gitlab.com/grmtech-oss/docker-images/ubuntu-nginx-gulp

cd /gt/gt-prog-repos/docker-images/ubuntu-nginx-gulp-php/
docker build --no-cache -t registry.gitlab.com/grmtech-oss/docker-images/ubuntu-nginx-gulp-php .
docker push registry.gitlab.com/grmtech-oss/docker-images/ubuntu-nginx-gulp-php

cd /gt/gt-prog-repos/docker-images/ubuntu-php/
docker build --no-cache -t registry.gitlab.com/grmtech-oss/docker-images/ubuntu-php .
docker push registry.gitlab.com/grmtech-oss/docker-images/ubuntu-php