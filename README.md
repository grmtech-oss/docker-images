Q1) What are the different image and what software is installed in each image?
https://docs.google.com/spreadsheets/d/1rsv_BIce3eWNJEs8aLzAes-1N0gMGhtAgUbh-o4P_bM/edit#gid=0

============================================================================================

Q2) How to get the latest image?

docker-compose up will not get the latest image if a local image already exists.
To get the latest image you need to execute:
docker-compose pull
docker-compose down
docker-compose up -d

============================================================================================

Q3) How to login to gitlab registry? Without this step you will get the access denied error, when you try to push the image to the registry.
root@9 /g/s/d/ubuntu# docker login registry.gitlab.com
Username: vikaskedia
Password:
Login Succeeded

============================================================================================

Q4) How to build and push to registry?
For ubuntu
----------
root@9 /g/s/d/ubuntu# docker build -t registry.gitlab.com/grmtech-oss/docker-images/ubuntu .
or
root@9 /g/s/d/ubuntu-nginx# docker build .
root@9 /g/s/d/ubuntu# docker push registry.gitlab.com/grmtech-oss/docker-images/ubuntu

For Ubuntu-nginx
---------------
root@9 /g/s/d/ubuntu-nginx# docker build -t registry.gitlab.com/grmtech-oss/docker-images/ubuntu-nginx .
root@9 /g/s/d/ubuntu-nginx# docker push registry.gitlab.com/grmtech-oss/docker-images/ubuntu-nginx

For Ubuntu-nginx-gulp
---------------------
root@9 /g/s/d/ubuntu-nginx# docker build -t registry.gitlab.com/grmtech-oss/docker-images/ubuntu-nginx-gulp .
root@9 /g/s/d/ubuntu-nginx# docker push registry.gitlab.com/grmtech-oss/docker-images/ubuntu-nginx-gulp

============================================================================================

Q5) How to test the image?
Option1:
-------
root@13 /g/s/d/ubuntu-nginx-gulp# docker run -it registry.gitlab.com/grmtech-oss/docker-images/ubuntu-nginx-gulp-php bash

Above command will not get the latest image if the image already exists locally.
To get the latest image:
root@13 /g/s/s/var-www-html# docker pull registry.gitlab.com/grmtech-oss/docker-images/ubuntu-php

Option2:
-------
root@13 /g/s/d/ubuntu-nginx-gulp# docker build .
Sending build context to Docker daemon  2.048kB
Step 1/4 : FROM registry.gitlab.com/grmtech-oss/docker-images/ubuntu-nginx
 ---> bd6d82287c80
Step 2/4 : RUN curl -sL https://deb.nodesource.com/setup_8.x | bash
 ---> Using cache
 ---> 2f846332e0c8
Step 3/4 : RUN apt-get install -y nodejs npm
 ---> Using cache
 ---> f56a0bc850dc
Step 4/4 : RUN npm install -g gulp gulp-clean-css gulp-uglify gulp-imagemin
 ---> Using cache
 ---> da203abe1028
Successfully built da203abe1028
root@13 /g/s/d/ubuntu-nginx-gulp# docker run -it da203abe1028 bash
root@5e3f7af3d440:/# node -v
bash: node: command not found
root@5e3f7af3d440:/# nodejs -v
v4.2.6

============================================================================================

jaikalima

