#!/bin/bash
docker pull registry.gitlab.com/grmtech-oss/docker-images/ubuntu:latest
docker pull registry.gitlab.com/grmtech-oss/docker-images/ubuntu-nginx:latest
docker pull registry.gitlab.com/grmtech-oss/docker-images/ubuntu-nginx-gulp:latest
docker pull registry.gitlab.com/grmtech-oss/docker-images/ubuntu-nginx-gulp-php:latest
docker pull registry.gitlab.com/grmtech-oss/docker-images/ubuntu-php:latest
